package com.example.pcsnow.entidades;


public class Pcs {

    String sigla, ciclo, docente;
    int año;
    byte[] archivo;

    public Pcs(String sigla, String docente, int año, String ciclo, byte[] archivo) {
        this.sigla = sigla;
        this.docente = docente;
        this.ciclo = ciclo;
        this.año = año;
        this.archivo = archivo;
    }

    public Pcs(String sigla, String docente, int año, String ciclo) {
        this.sigla = sigla;
        this.docente = docente;
        this.ciclo = ciclo;
        this.año = año;
    }

    public Pcs(String sigla, String docente) {
        this.sigla = sigla;
        this.docente = docente;
    }

    public Pcs(String sigla, String docente, int año) {
        this.año = año;
        this.sigla = sigla;
        this.docente = docente;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public String getDocente() {
        return docente;
    }

    public void setDocente(String docente) {
        this.docente = docente;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public boolean igualCusoProfe(Pcs pcs) {
        return this.sigla.equals(pcs.getSigla()) && this.docente.equals(pcs.getDocente());
    }

    public boolean igualCursoProfeAño(Pcs pcs) {
        return this.sigla.equals(pcs.getSigla()) && this.docente.equals(pcs.getDocente()) && this.año==pcs.getAño();
    }

    public static final String TABLA_PCS = "pcs";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_SIGLA = "sigla_id";
    public static final String CAMPO_DOCENTE = "docente_id";
    public static final String CAMPO_AÑO = "año";
    public static final String CAMPO_CICLO = "ciclo";
    public static final String CAMPO_ARCHIVO = "archivo";

    public static final String CREAR_TABLA_PCS = "CREATE TABLE " + TABLA_PCS + " (" + CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + CAMPO_SIGLA + " INTEGER, " + CAMPO_DOCENTE + " INTEGER, " + CAMPO_AÑO + " INTEGER, " + CAMPO_CICLO + " TEXT, " + CAMPO_ARCHIVO + " BLOB," +
            " FOREIGN KEY (" + CAMPO_DOCENTE + ") REFERENCES " + Profesor.TABLA_PROFESORES + " (" + Profesor.CAMPO_ID + "), FOREIGN KEY (" + CAMPO_SIGLA + ") REFERENCES " + Curso.TABLA_CURSOS + " (" + Curso.CAMPO_ID +")); ";
    public static final String DROPEAR_TABLA_PCS = "DROP TABLE IF EXISTS " + TABLA_PCS;
}

package com.example.pcsnow.entidades;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Conexion extends SQLiteOpenHelper {
    public static final String BD_APP = "bd_pcsnow";
    String[] profes, siglas, cursenames, userNombre, userApellidos, userEmail, username, userPassword;
    int[] creditos;
    public Conexion(@Nullable Context context) {
        super(context, BD_APP, null, 1);
        profes = new String[] {"Jorge Ruiz","Cesar Angulo","Rolando Seclén"};

        cursenames = new String[] {"Programación Básica","Investigación de Operaciones 2","Tecnología Eléctrica 1"};
        siglas = new String[] {"PB","O2","TE1"};
        creditos = new int[] {4,4,5};

        userNombre = new String[] {"Jean Pierre","Eduardo Jesús","Andre"};
        userApellidos = new String[] {"Flores Regalado","Bravo Diaz","La Torre Peña"};
        userEmail = new String[] {"jean.flores@alum.udep.edu.pe","eduardo.bravo@alum.udep.edu.pe","andre.latorre@alum.udep.edu.pe"};
        username = new String[] {"Pier1671","OdraudeMax","AndreCash"};
        userPassword = new String[] {"j1234","o1234","a1234"};
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Usuario.CREAR_TABLA_USUARIOS);
        sqLiteDatabase.execSQL(Pcs.CREAR_TABLA_PCS);
        sqLiteDatabase.execSQL(Curso.CREAR_TABLA_CURSOS);
        sqLiteDatabase.execSQL(Profesor.CREAR_TABLA_PROFESORES);
        for (int i=0; i<profes.length; i++) {
            sqLiteDatabase.execSQL(Profesor.INSERTAR_DATOS_PROFES + " VALUES ('" + profes[i] + "')");
        }
        for (int i=0; i<userNombre.length; i++) {
            sqLiteDatabase.execSQL(Usuario.INSERTAR_DATOS_USUARIOS + " VALUES ('" + userNombre[i] + "', '" + userApellidos[i] + "', '" + userEmail[i] + "', '" + username[i] + "', '" + userPassword[i] +"')");
        }
        for (int i=0; i<cursenames.length; i++) {
            sqLiteDatabase.execSQL(Curso.INSERTAR_DATOS_CURSOS + " VALUES ('" + cursenames[i] + "', '" + siglas[i] + "', " + creditos[i] + ")");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(Usuario.DROPEAR_TABLA_USUARIOS);
        sqLiteDatabase.execSQL(Pcs.DROPEAR_TABLA_PCS);
        sqLiteDatabase.execSQL(Curso.DROPEAR_TABLA_CURSOS);
        sqLiteDatabase.execSQL(Profesor.DROPEAR_TABLA_PROFESORES);
        onCreate(sqLiteDatabase);
    }
}

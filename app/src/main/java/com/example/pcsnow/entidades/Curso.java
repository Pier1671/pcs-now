package com.example.pcsnow.entidades;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Curso {
    private String cursename, sigla;
    private int creditos;
    private int id;
    private List<Curso> cursos;

    public Curso(int id, String cursename, String sigla, int creditos) {
        this.id = id;
        this.cursename = cursename;
        this.sigla = sigla;
        this.creditos = creditos;
    }

    public Curso(String cursename, String sigla, int creditos) {
        this.cursename = cursename;
        this.sigla = sigla;
        this.creditos = creditos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Curso> getCursos() { return cursos; }

    public void setCursos(List<Curso> cursos) { this.cursos = cursos; }

    public String getCursename() { return cursename; }

    public void setCursename(String cursename) {
        this.cursename = cursename;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public List<String> getListaCursos() {
        List<String> curseNames;
        curseNames = new ArrayList<String>();
        for (Curso c : cursos) {
            curseNames.add(c.getSigla());
        }
        return curseNames;
    }

    public void agregarCurso(String cursename, String sigla, int creditos) {
        cursos.add(new Curso(cursename, sigla, creditos));
    }

    public boolean validarNameCurso(String cursename) {
        boolean f = false;
        for (Curso c : cursos) {
            f = c.getCursename().equals(cursename);;
            if (f) break;
        }
        return !f;
    }

    public boolean validarSigla(String sigla) {
        boolean f = false;
        for (Curso c : cursos) {
            f = c.getSigla().equals(sigla);;
            if (f) break;
        }
        return !f;
    }

    public static final String TABLA_CURSOS = "cursos";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_CURSENAME = "cursename";
    public static final String CAMPO_SIGLA = "sigla";
    public static final String CAMPO_CREDITOS = "creditos";

    public static final String CREAR_TABLA_CURSOS = "CREATE TABLE " + TABLA_CURSOS + " (" + CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + CAMPO_CURSENAME + " TEXT, " + CAMPO_SIGLA + " TEXT, " + CAMPO_CREDITOS + " INTEGER)";
    public static final String DROPEAR_TABLA_CURSOS = "DROP TABLE IF EXISTS " + TABLA_CURSOS;
    public static final String INSERTAR_DATOS_CURSOS = "INSERT INTO " + TABLA_CURSOS + " (" + CAMPO_CURSENAME + ", " + CAMPO_SIGLA + ", " + CAMPO_CREDITOS +")";
}

package com.example.pcsnow.entidades;

import java.io.Serializable;
import java.util.List;

public class Usuario implements Serializable {

    private String nombre, apellido, correo, usuario, contraseña;

    public Usuario(String usuario, String contraseña) {
        this.usuario = usuario;
        this.contraseña = contraseña;
    }

    public Usuario(String nombre, String apellido, String correo, String usuario) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.usuario = usuario;
    }

    public Usuario(String nombre, String apellido, String correo, String usuario, String contraseña){
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.usuario = usuario;
        this.contraseña = contraseña;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public boolean igualLogin(Usuario u) {
        return this.usuario.equals(u.getUsuario()) && this.contraseña.equals(u.getContraseña());
    }

    public boolean igualRegistro(Usuario u) {
        return this.nombre.equals(u.getNombre()) && this.apellido.equals(u.getApellido());
    }

    public static final String TABLA_USUARIOS = "usuarios";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_NOMBRE = "nombre";
    public static final String CAMPO_APELLIDO = "apellido";
    public static final String CAMPO_CORREO = "correo";
    public static final String CAMPO_USUARIO = "usuario";
    public static final String CAMPO_CONTRASEÑA = "contraseña";
    public static final String CAMPO_FOTO = "foto";

    public static final String CREAR_TABLA_USUARIOS = "CREATE TABLE " + TABLA_USUARIOS + " (" + CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + CAMPO_NOMBRE + " TEXT, " + CAMPO_APELLIDO + " TEXT, " + CAMPO_CORREO + " TEXT, " + CAMPO_USUARIO + " TEXT, " + CAMPO_CONTRASEÑA + " TEXT, " + CAMPO_FOTO + " BLOB)";
    public static final String DROPEAR_TABLA_USUARIOS = "DROP TABLE IF EXISTS " + TABLA_USUARIOS;
    public static final String INSERTAR_DATOS_USUARIOS = "INSERT INTO " + TABLA_USUARIOS + " (" + CAMPO_NOMBRE + ", " + CAMPO_APELLIDO + ", " + CAMPO_CORREO + ", " + CAMPO_USUARIO + ", " + CAMPO_CONTRASEÑA +")";
}

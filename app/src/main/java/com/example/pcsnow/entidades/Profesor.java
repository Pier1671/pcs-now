package com.example.pcsnow.entidades;

public class Profesor {
    private int id;
    private String docente;

    public Profesor(int id, String docente) {
        this.id=id;
        this.docente=docente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDocente() {
        return docente;
    }

    public void setDocente(String docente) {
        this.docente = docente;
    }

    public static final String TABLA_PROFESORES = "profesores";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_DOCENTE = "docente";

    public static final String CREAR_TABLA_PROFESORES = "CREATE TABLE " + TABLA_PROFESORES + " (" + CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + CAMPO_DOCENTE + " TEXT)";
    public static final String DROPEAR_TABLA_PROFESORES = "DROP TABLE IF EXISTS " + TABLA_PROFESORES;
    public static final String INSERTAR_DATOS_PROFES = "INSERT INTO " + TABLA_PROFESORES + " (" + CAMPO_DOCENTE + ")";
}

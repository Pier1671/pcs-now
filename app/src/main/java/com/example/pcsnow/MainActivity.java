package com.example.pcsnow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.pcsnow.entidades.Usuario;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button botonLogin, botonSign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("pcsnow", MODE_PRIVATE);
        Boolean closedSesion = sharedPreferences.getBoolean("closedSesion", true);
        if (!closedSesion) {
            Intent i = new Intent(this, HomeActivity.class);
            startActivity(i);
        }

        botonLogin = (Button) findViewById(R.id.botonLogin);
        botonSign = (Button) findViewById(R.id.botonSign);

        botonLogin.setOnClickListener(this);
        botonSign.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.botonLogin) {
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
        } else if (view.getId() == R.id.botonSign){
            Intent i = new Intent(this, SignupActivity.class);
            startActivity(i);
        }
    }
}
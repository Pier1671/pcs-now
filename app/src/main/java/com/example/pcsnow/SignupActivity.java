package com.example.pcsnow;

import androidx.annotation.ColorRes;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pcsnow.entidades.Conexion;
import com.example.pcsnow.entidades.Usuario;
import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener{

    Conexion conexion;
    private Button botonSignup;
    private CheckBox checkboxTerms;
    private EditText editNombre, editApellidos, editCorreo, editUser, editPassword, editConfirmar;
    private ImageButton imageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        botonSignup = (Button) findViewById(R.id.botonSignup);
        checkboxTerms = (CheckBox) findViewById(R.id.checkboxTerms);
        editNombre = (EditText) findViewById(R.id.editNombre);
        editApellidos = (EditText) findViewById(R.id.editApellidos);
        editCorreo = (EditText) findViewById(R.id.editCorreo);
        editUser = (EditText) findViewById(R.id.editUser);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editConfirmar = (EditText) findViewById(R.id.editConfirmar);
        imageBack = (ImageButton) findViewById(R.id.imageBack) ;

        botonSignup.setOnClickListener(this);
        editCorreo.setOnFocusChangeListener(this);
        imageBack.setOnClickListener(this);

        getSupportActionBar().setTitle(getResources().getString(R.string.sign));

        conexion = new Conexion(this);
    }

    @Override
    public void onClick(View view) {
        String eNombre = editNombre.getText().toString();
        String eApellidos = editApellidos.getText().toString();
        String eCorreo = editCorreo.getText().toString();
        String eUser = editUser.getText().toString();
        String ePassword = editPassword.getText().toString();
        String eConfirmar = editConfirmar.getText().toString();
        getLista();

        if (view.getId() == R.id.imageBack) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }
        if (view.getId() == R.id.botonSignup) {
            if (eNombre.equals("") || eApellidos.equals("") || eCorreo.equals("") || eUser.equals("") || ePassword.equals("") || eConfirmar.equals("")) {
                Toast.makeText(this, "Complete todos sus datos", Toast.LENGTH_LONG).show();
                if (eNombre.equals("")) {
                    editNombre.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eApellidos.equals("")) {
                    editApellidos.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eCorreo.equals("")) {
                    editCorreo.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eUser.equals("")) {
                    editUser.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (ePassword.equals("")) {
                    editPassword.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eConfirmar.equals("")) {
                    editConfirmar.setHintTextColor(getResources().getColor(R.color.red));
                }
            }
            else if (ePassword.equals(eConfirmar) && validarRegistro(eNombre, eApellidos, eCorreo, eUser) && checkboxTerms.isChecked() && !eUser.contains(" ")) {
                Intent i = new Intent(this, HomeActivity.class);
                Usuario usuario = new Usuario(eNombre,eApellidos,eCorreo,eUser,ePassword);

                SharedPreferences.Editor editor = getSharedPreferences("pcsnow", MODE_PRIVATE).edit();
                Gson gson = new Gson();
                String json1 = gson.toJson(usuario);
                editor.putString("saveUsuario", json1);
                editor.commit();

                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(Usuario.CAMPO_NOMBRE, eNombre);
                values.put(Usuario.CAMPO_APELLIDO, eApellidos);
                values.put(Usuario.CAMPO_CORREO, eCorreo);
                values.put(Usuario.CAMPO_USUARIO, eUser);
                values.put(Usuario.CAMPO_CONTRASEÑA, ePassword);
                db.insert(Usuario.TABLA_USUARIOS, Usuario.CAMPO_ID, values);

                startActivity(i);
            }
            else {
                editPassword.setText("");
                editConfirmar.setText("");
                editPassword.setHintTextColor(getResources().getColor(R.color.gris));
                editConfirmar.setHintTextColor(getResources().getColor(R.color.gris));

                if (!validarFullName(new Usuario(eNombre, eApellidos, eCorreo, eUser))) {
                    Toast.makeText(this, "El Nombre y Apellido ingresado ya existe", Toast.LENGTH_LONG).show();
                    editNombre.setText("");
                    editApellidos.setText("");
                    editNombre.setHintTextColor(getResources().getColor(R.color.red));
                    editApellidos.setHintTextColor(getResources().getColor(R.color.red));
                }
                else if (!validarCorreo(new Usuario(eNombre, eApellidos, eCorreo, eUser))) {
                    Toast.makeText(this, "El Correo ingresado ya existe", Toast.LENGTH_LONG).show();
                    editCorreo.setText("");
                    editCorreo.setHintTextColor(getResources().getColor(R.color.red));
                }
                else if (!validarUsername(new Usuario(eNombre, eApellidos, eCorreo, eUser))) {
                    Toast.makeText(this, "El Usuario ingresado ya existe", Toast.LENGTH_LONG).show();
                    editUser.setText("");
                    editUser.setHintTextColor(getResources().getColor(R.color.red));
                }
                else if (!ePassword.equals(eConfirmar)) {
                    Toast.makeText(this, "Confirmación de contraseña incorrecta", Toast.LENGTH_LONG).show();
                    editPassword.setHintTextColor(getResources().getColor(R.color.red));
                    editConfirmar.setHintTextColor(getResources().getColor(R.color.red));
                }
                else if (!checkboxTerms.isChecked()) {
                    Toast.makeText(this, "Debe aceptar los Términos y Condiciones", Toast.LENGTH_LONG).show();
                }

            }

        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        String eCorreo = editCorreo.getText().toString();
        if (view.getId() == R.id.editCorreo && eCorreo.equals("") && b) {
            editCorreo.setText("@alum.udep.edu.pe");
        }
    }

    List<Usuario> listaUsuarios;
    private void getLista() {

        listaUsuarios = new LinkedList<Usuario>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Usuario.CAMPO_NOMBRE, Usuario.CAMPO_APELLIDO, Usuario.CAMPO_CORREO, Usuario.CAMPO_USUARIO};
        Cursor cursor = db.query(Usuario.TABLA_USUARIOS, campos, null,null,null,null,Usuario.CAMPO_ID);

        while (cursor.moveToNext()) {
            Usuario u = new Usuario(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
            listaUsuarios.add(u);
        }
    }

    public boolean validarFullName(Usuario usuario) {
        boolean valNombres = false;
        for (Usuario u : listaUsuarios) {
            valNombres = u.igualRegistro(usuario);
            if (valNombres) break;
        }
        return !valNombres;
    }

    public boolean validarCorreo(Usuario usuario) {
        boolean valCorreo = false;
        for (Usuario u : listaUsuarios) {
            valCorreo = u.getCorreo().equals(usuario.getCorreo());;
            if (valCorreo) break;
        }
        return !valCorreo;
    }

    public boolean validarUsername(Usuario usuario) {
        boolean valUsername = false;
        for (Usuario u : listaUsuarios) {
            valUsername = u.getUsuario().equals(usuario.getUsuario());
            if (valUsername) break;
        }
        return !valUsername;
    }

    public boolean validarRegistro(Usuario usuario) {
        return (validarFullName(usuario) && validarCorreo(usuario) && validarUsername(usuario));
    }
    public boolean validarRegistro(String nombre, String apellido, String correo, String usuario){
        return validarRegistro(new Usuario(nombre, apellido, correo, usuario));
    }
}
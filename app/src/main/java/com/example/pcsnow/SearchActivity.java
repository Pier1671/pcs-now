package com.example.pcsnow;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pcsnow.entidades.Conexion;
import com.example.pcsnow.entidades.Curso;
import com.example.pcsnow.entidades.Pcs;
import com.example.pcsnow.entidades.Profesor;
import com.example.pcsnow.entidades.Usuario;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    Conexion conexion;
    byte[] blobPc;
    String nombrePc;
    private Spinner spinnerFiltro, spinnerSegundo, spinnerAño, spinnerCiclo;
    private Button botonSearch;
    private RadioButton radioCurso, radioProfesor;
    private ImageButton imageBack4;
    private TextView textViewF1, textViewF2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        conexion = new Conexion(this);

        spinnerFiltro = (Spinner) findViewById(R.id.spinnerFiltro);
        spinnerSegundo = (Spinner) findViewById(R.id.spinnerSegundo);
        spinnerAño = (Spinner) findViewById(R.id.spinnerAño);
        spinnerCiclo = (Spinner) findViewById(R.id.spinnerCiclo);
        botonSearch = (Button) findViewById(R.id.botonSearch);
        radioCurso = (RadioButton) findViewById(R.id.radioCurso);
        radioProfesor = (RadioButton) findViewById(R.id.radioProfesor);
        imageBack4 = (ImageButton) findViewById(R.id.imageBack4);
        textViewF1 = (TextView) findViewById(R.id.textViewF1);
        textViewF2 = (TextView) findViewById(R.id.textViewF2);

        radioCurso.setOnClickListener(this);
        radioProfesor.setOnClickListener(this);
        imageBack4.setOnClickListener(this);
        botonSearch.setOnClickListener(this);
        spinnerFiltro.setOnItemSelectedListener(this);
        spinnerSegundo.setOnItemSelectedListener(this);
        spinnerAño.setOnItemSelectedListener(this);

        getSupportActionBar().setTitle("Buscar Pc");

        getListaPcs();
    }

    private static int REQUEST_CODE_WRITE_IMAGE_STORAGE_PERMISSION = 1;
    private static int REQUEST_CODE_WRITE_FILE_STORAGE_PERMISSION = 3;

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.radioCurso) {
            radioProfesor.setChecked(false);
            textViewF1.setText("Curso");
            textViewF2.setText("Profesor");
            ArrayAdapter<String> spinnerFiltroAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getListaCursos());
            spinnerFiltro.setAdapter(spinnerFiltroAdapter);
        }
        else if (view.getId() == R.id.radioProfesor) {
            radioCurso.setChecked(false);
            textViewF2.setText("Curso");
            textViewF1.setText("Profesor");
            ArrayAdapter<String> spinnerFiltroAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getlistaProfes());
            spinnerFiltro.setAdapter(spinnerFiltroAdapter);
        }
        else if (view.getId() == R.id.imageBack4) {
            Intent i = new Intent(this, HomeActivity.class);
            startActivity(i);
        }
        else if (view.getId() == R.id.botonSearch) {
            if (!radioProfesor.isChecked() && !radioCurso.isChecked()) {
                Toast.makeText(this, "Seleccione una Pc", Toast.LENGTH_LONG).show();
            }
            else if (spinnerSegundo.getSelectedItem()==null) {
                Toast.makeText(this, "Pc no encontrada", Toast.LENGTH_LONG).show();
            }
            else {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SearchActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_IMAGE_STORAGE_PERMISSION);
                } else {
                    String itemCiclo = spinnerCiclo.getSelectedItem().toString();
                    int itemAño = (int) spinnerAño.getSelectedItem();
                    itemCiclo = itemCiclo.replaceAll(itemAño+"-", "");
                    if (radioCurso.isChecked()) {
                        String itemCurso = spinnerFiltro.getSelectedItem().toString();
                        String itemDocente = spinnerSegundo.getSelectedItem().toString();
                        getArchivo(new Pcs(itemCurso,itemDocente,itemAño),itemCiclo);

                        nombrePc = itemCurso + "_" + spinnerCiclo.getSelectedItem().toString();
                        Toast.makeText(this, "Pc Seleccionada: " + nombrePc, Toast.LENGTH_LONG).show();
                    }
                    else {
                        String itemCurso = spinnerSegundo.getSelectedItem().toString();
                        String itemDocente = spinnerFiltro.getSelectedItem().toString();
                        getArchivo(new Pcs(itemCurso,itemDocente,itemAño),itemCiclo);

                        nombrePc = itemCurso + "_" + spinnerCiclo.getSelectedItem().toString();
                        Toast.makeText(this, "Pc Seleccionada: " + nombrePc, Toast.LENGTH_LONG).show();
                    }
                    guardarPc();
                }
            }
        }
    }

    private void guardarPc() {
        guardarArchivo(nombrePc+".pdf",blobPc);
    }

    private void guardarArchivo(String fileName, byte[] blob) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),fileName);

        try {

            if (!file.exists()) file.createNewFile();

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(blob);
            fos.close();
            Toast.makeText(this,"Se guardó en " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();

        } catch (IOException e) {
            Toast.makeText(this,e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_WRITE_FILE_STORAGE_PERMISSION && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                guardarPc();
            } else {
                Toast.makeText(this, "Permiso denegado", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapterView.getId() == R.id.spinnerFiltro) {
            String itemFiltro = adapterView.getItemAtPosition(i)+"";
            if (radioCurso.isChecked()) {
                ArrayAdapter<String> spinnerSegundoAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getListaDocente(itemFiltro));
                spinnerSegundo.setAdapter(spinnerSegundoAdapter);
            }
            else if(radioProfesor.isChecked()) {
                ArrayAdapter<String> spinnerSegundoAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getListaSigla(itemFiltro));
                spinnerSegundo.setAdapter(spinnerSegundoAdapter);
            }
            spinnerAño.setAdapter(null);
            spinnerCiclo.setAdapter(null);
        }
        if (adapterView.getId() == R.id.spinnerSegundo) {
            String itemSegundo = adapterView.getItemAtPosition(i)+"";
            String itemFiltro = spinnerFiltro.getSelectedItem()+"";
            if (radioCurso.isChecked()) {
                ArrayAdapter<Integer> spinnerAñoAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, getListaAño(new Pcs(itemFiltro,itemSegundo)));
                spinnerAño.setAdapter(spinnerAñoAdapter);
            }
            else if(radioProfesor.isChecked()) {
                ArrayAdapter<Integer> spinnerAñoAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, getListaAño(new Pcs(itemSegundo, itemFiltro)));
                spinnerAño.setAdapter(spinnerAñoAdapter);
            }
        }

        if (adapterView.getId() == R.id.spinnerAño) {
            Integer itemAño = (Integer) adapterView.getItemAtPosition(i);
            String itemSegundo = spinnerSegundo.getSelectedItem()+"";
            String itemFiltro = spinnerFiltro.getSelectedItem()+"";
            if (radioCurso.isChecked()) {
                ArrayAdapter<String> spinnerCicloAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getListaCiclo(new Pcs(itemFiltro,itemSegundo, itemAño)));
                spinnerCiclo.setAdapter(spinnerCicloAdapter);
            }
            else if(radioProfesor.isChecked()) {
                ArrayAdapter<String> spinnerCicloAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getListaCiclo(new Pcs(itemSegundo, itemFiltro, itemAño)));
                spinnerCiclo.setAdapter(spinnerCicloAdapter);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    List<Pcs> listaPcs;

    private void getListaPcs() {

        listaPcs = new LinkedList<Pcs>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String sqlCode = "SELECT " + Curso.TABLA_CURSOS+ "."+Curso.CAMPO_SIGLA +", " + Profesor.TABLA_PROFESORES+ "."+Profesor.CAMPO_DOCENTE +", " + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_AÑO +", " + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_CICLO +", " + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_ARCHIVO
                + " FROM " + Pcs.TABLA_PCS
                + " INNER JOIN " + Curso.TABLA_CURSOS + " ON "+ Curso.TABLA_CURSOS+ "."+Curso.CAMPO_ID + "=" + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_SIGLA
                + " INNER JOIN " + Profesor.TABLA_PROFESORES + " ON "+ Profesor.TABLA_PROFESORES+ "."+Profesor.CAMPO_ID + "=" + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_DOCENTE +";";
        Cursor cursor = db.rawQuery(sqlCode,null);
        while (cursor.moveToNext()) {
            Pcs p = new Pcs(cursor.getString(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3), cursor.getBlob(4));
            listaPcs.add(p);
        }
    }

    public List<String> getListaDocente(String sigla) {
        SQLiteDatabase db = conexion.getReadableDatabase();
        List<String> relDoc;
        relDoc = new ArrayList<String>();
        String sqlCode = "SELECT DISTINCT " + Profesor.TABLA_PROFESORES+ "."+Profesor.CAMPO_DOCENTE
                + " FROM " + Profesor.TABLA_PROFESORES + " INNER JOIN " + Pcs.TABLA_PCS + " ON "+ Profesor.TABLA_PROFESORES+ "."+Profesor.CAMPO_ID + "=" + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_DOCENTE
                + " WHERE " + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_SIGLA + "= (SELECT "+ Curso.CAMPO_ID + " FROM " + Curso.TABLA_CURSOS
                + " WHERE " +  Curso.CAMPO_SIGLA+ "='"+sigla+"');";
        Cursor cursor = db.rawQuery(sqlCode,null);
        while (cursor.moveToNext()) {
            relDoc.add(cursor.getString(0));
        }
        return relDoc;
    }

    public List<String> getListaSigla(String docente) {
        SQLiteDatabase db = conexion.getReadableDatabase();
        List<String> relSig;
        relSig = new ArrayList<String>();
        String sqlCode = "SELECT DISTINCT " + Curso.TABLA_CURSOS+ "."+Curso.CAMPO_SIGLA
                + " FROM " + Curso.TABLA_CURSOS + " INNER JOIN " + Pcs.TABLA_PCS + " ON "+ Curso.TABLA_CURSOS+ "."+Curso.CAMPO_ID + "=" + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_SIGLA
                + " WHERE " + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_DOCENTE + "= (SELECT "+ Profesor.CAMPO_ID + " FROM " + Profesor.TABLA_PROFESORES
                + " WHERE " +  Profesor.CAMPO_DOCENTE+ "='"+docente+"');";
        Cursor cursor = db.rawQuery(sqlCode,null);
        while (cursor.moveToNext()) {
            relSig.add(cursor.getString(0));
        }
        return relSig;
    }

    public List<Integer> getListaAño(Pcs pcsObjeto) {
        List<Integer> relAño;
        relAño = new ArrayList<Integer>();
        for (Pcs p: listaPcs) {
            if (p.igualCusoProfe(pcsObjeto) && !relAño.contains(p.getAño())) {
                relAño.add(p.getAño());
            }
        }
        return relAño;
    }

    public List<String> getListaCiclo(Pcs pcsObjeto) {
        List<String> relCic;
        relCic = new ArrayList<String>();
        for (Pcs p: listaPcs) {
            if (p.igualCursoProfeAño(pcsObjeto)) {
                relCic.add(p.getAño()+"-"+p.getCiclo());
            }
        }
        return relCic;
    }

    public void getArchivo(Pcs pcsObjeto, String ciclo) {
        for (Pcs p: listaPcs) {
            if (p.igualCursoProfeAño(pcsObjeto) && p.getCiclo().equals(ciclo)) {
                blobPc = p.getArchivo();
            }
        }
    }

    List<String> listaCursos;

    private List<String> getListaCursos() {

        listaCursos = new LinkedList<String>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Curso.CAMPO_SIGLA};
        Cursor cursor = db.query(Curso.TABLA_CURSOS, campos, null,null,null,null,Curso.CAMPO_ID);

        while (cursor.moveToNext()) {
            listaCursos.add(cursor.getString(0));
        }
        return listaCursos;
    }

    List<String> listaProfes;

    private List<String> getlistaProfes() {

        listaProfes = new LinkedList<String>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Profesor.CAMPO_DOCENTE};
        Cursor cursor = db.query(Profesor.TABLA_PROFESORES, campos, null,null,null,null,Profesor.CAMPO_ID);

        while (cursor.moveToNext()) {
            listaProfes.add(cursor.getString(0));
        }
        return listaProfes;
    }
}
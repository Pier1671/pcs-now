package com.example.pcsnow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pcsnow.entidades.Conexion;
import com.example.pcsnow.entidades.Curso;
import com.example.pcsnow.entidades.Usuario;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener{

    Conexion conexion;
    Bitmap foto = null;
    private TextView textHome, textInfo1, textInfo2, textInfo3, textInfo4;
    private RadioButton radioPerfil, radioFuncion, radioHelp;
    private Button botonBuscar, botonSubir, botonSettings;
    private ImageButton imageBack3;
    private ImageView fotoPerfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        conexion = new Conexion(this);

        SharedPreferences.Editor editor = getSharedPreferences("pcsnow", MODE_PRIVATE).edit();
        editor.putBoolean("closedSesion", false);

        SharedPreferences sharedPreferences = getSharedPreferences("pcsnow", MODE_PRIVATE);
        String json = sharedPreferences.getString("saveUsuario", "");

        textHome = (TextView) findViewById(R.id.textHome);
        Gson gson = new Gson();
        Usuario usuario = gson.fromJson(json, Usuario.class);

        String name = usuario.getNombre();
        textHome.setText("Bienvenido " + name + "!");

        textInfo1 = (TextView) findViewById(R.id.textInfo1);
        textInfo2 = (TextView) findViewById(R.id.textInfo2);
        textInfo3 = (TextView) findViewById(R.id.textInfo3);
        textInfo4 = (TextView) findViewById(R.id.textInfo4);
        radioPerfil = (RadioButton) findViewById(R.id.radioPerfil);
        radioFuncion = (RadioButton) findViewById(R.id.radioFuncion);
        radioHelp = (RadioButton) findViewById(R.id.radioHelp);
        botonBuscar = (Button) findViewById(R.id.botonBuscar);
        botonSubir = (Button) findViewById(R.id.botonSubir);
        botonSettings = (Button) findViewById(R.id.botonSettings);
        imageBack3 = (ImageButton) findViewById(R.id.imageBack3);
        fotoPerfil = (ImageView) findViewById(R.id.fotoPerfil);

        botonBuscar.setOnClickListener(this);
        botonSubir.setOnClickListener(this);
        radioPerfil.setOnClickListener(this);
        radioFuncion.setOnClickListener(this);
        radioHelp.setOnClickListener(this);
        imageBack3.setOnClickListener(this);
        botonSettings.setOnClickListener(this);
        fotoPerfil.setOnClickListener(this);

        if (getFoto(usuario.getUsuario())!=null) {
            fotoPerfil.setImageBitmap(getFoto(usuario.getUsuario()));
        }

        getSupportActionBar().setTitle("Home");

        editor.commit();
    }

    private static final int REQUEST_CODE_STORAGE_PERMISSION = 1;
    private static final int REQUEST_CODE_SELECT_IMAGE = 2;

    @Override
    public void onClick(View view) {
        SharedPreferences sharedPreferences = getSharedPreferences("pcsnow", MODE_PRIVATE);
        String json = sharedPreferences.getString("saveUsuario", "");
        Gson gson = new Gson();
        Usuario usuario = gson.fromJson(json, Usuario.class);

        String name = usuario.getNombre();
        String apellido = usuario.getApellido();
        String correo = usuario.getCorreo();
        String user = usuario.getUsuario();

        if (view.getId() == R.id.radioPerfil) {
            radioHelp.setChecked(false);
            radioFuncion.setChecked(false);
            textInfo1.setText("Nombre: " + name);
            textInfo2.setText("Apellido: " + apellido);
            textInfo3.setText("Correo: " + correo);
            textInfo4.setText("Usuario: " + user);
        }
        else if (view.getId() == R.id.radioFuncion) {
            radioHelp.setChecked(false);
            radioPerfil.setChecked(false);
            textInfo1.setText("Primero selecciona la actividad deseada");
            textInfo2.setText("Buscar: Para encontrar una Pc en específico");
            textInfo3.setText("Subir: Compartir tu Pc con los demás");
            textInfo4.setText("Interactuar: Debatir y apoyar a otros Usuarios");
        }
        else if (view.getId() == R.id.radioHelp) {
            textInfo4.setText("");
            radioPerfil.setChecked(false);
            radioFuncion.setChecked(false);
            textInfo1.setText("Si tienes problemas con la app,");
            textInfo2.setText("repotar las deficiencias en el siguiente email:");
            textInfo3.setText("jeanpierre220102@gmail.com");
        }
        else if (view.getId() == R.id.imageBack3) {
            Intent i = new Intent(this, MainActivity.class);
            SharedPreferences.Editor editor = getSharedPreferences("pcsnow", MODE_PRIVATE).edit();
            editor.putBoolean("closedSesion", true);
            editor.commit();
            startActivity(i);
        }
        else if (view.getId() == R.id.botonBuscar) {
            Intent i = new Intent(this, SearchActivity.class);
            startActivity(i);
        }
        else if (view.getId() == R.id.botonSubir) {
            Intent i = new Intent(this, UploadActivity.class);
            startActivity(i);
        }
        else if (view.getId() == R.id.botonSettings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
        }
        else if (view.getId() == R.id.fotoPerfil) {
            if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_STORAGE_PERMISSION);

            } else {
                seleccionarImagen();
            }
        }
    }

    public void seleccionarImagen() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if(intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CODE_SELECT_IMAGE);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);

        if(requestCode == REQUEST_CODE_STORAGE_PERMISSION && grantResults.length>0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                seleccionarImagen();
            }
            else {
                Toast.makeText(this,"Permiso Denegado",Toast.LENGTH_LONG).show();
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK) {
            if (data!=null && data.getData()!=null) {

                try {

                    Uri selectedImageUri = data.getData();
                    InputStream in = getContentResolver().openInputStream(selectedImageUri);
                    Bitmap bitmap = BitmapFactory.decodeStream(in);
                    fotoPerfil.setImageBitmap(bitmap);
                    this.foto = bitmap;

                    if (this.foto != null) {
                        byte[] bytes;
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        this.foto.compress(Bitmap.CompressFormat.JPEG,100, out);
                        bytes = out.toByteArray();

                        SharedPreferences sharedPreferences = getSharedPreferences("pcsnow", MODE_PRIVATE);
                        String json = sharedPreferences.getString("saveUsuario", "");
                        Gson gson = new Gson();
                        Usuario usuario = gson.fromJson(json, Usuario.class);

                        SQLiteDatabase db = conexion.getWritableDatabase();
                        String[] valoresWhere = {usuario.getUsuario()};
                        String where = Usuario.CAMPO_USUARIO + "=?";
                        ContentValues values = new ContentValues();
                        values.put(Usuario.CAMPO_FOTO, bytes);
                        db.update(Usuario.TABLA_USUARIOS, values, where,valoresWhere);
                        Toast.makeText(this,"Foto Actualizada",Toast.LENGTH_LONG).show();
                    }

                } catch (FileNotFoundException e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        }
    }

    public Bitmap getFoto(String usuario) {
        Bitmap bitmap = null;
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] valoresWhere = {usuario};
        String where = Usuario.CAMPO_USUARIO + "=?";
        String[] campos = {Usuario.CAMPO_FOTO};
        Cursor cursor = db.query(Usuario.TABLA_USUARIOS, campos, where,valoresWhere,null,null,Usuario.CAMPO_ID);

        while (cursor.moveToNext()) {
            if (cursor.getBlob(0) != null) {
                byte[] bytes = cursor.getBlob(0);
                bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            }
        }
        return bitmap;
    }
}
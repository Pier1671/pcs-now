package com.example.pcsnow;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.CompoundButtonCompat;

import android.Manifest;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.sax.TextElementListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pcsnow.entidades.Conexion;
import com.example.pcsnow.entidades.Curso;
import com.example.pcsnow.entidades.Pcs;
import com.example.pcsnow.entidades.Profesor;
import com.example.pcsnow.entidades.Usuario;
import com.google.android.material.internal.TextWatcherAdapter;
import com.google.android.material.slider.RangeSlider;
import com.google.android.material.slider.Slider;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UploadActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    Spinner spinnerCurso, spinnerProfesor, spinnerAddCiclo;
    CheckBox checkboxAddCurso, checkboxAddProfesor;
    EditText editTextCurso, editTextSigla, editTextCreditos, editTextProfesor, editTextAñoPc;
    Button botonAñadirCurso, botonAñadirProfesor, botonPrincipal, addArchivo;
    ImageButton imageBack5;
    NotificationCompat.Builder builder;
    Conexion conexion;
    byte[] blobPc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        conexion = new Conexion(this);

        spinnerCurso = (Spinner) findViewById(R.id.spinnerCurso);
        spinnerProfesor = (Spinner) findViewById(R.id.spinnerProfesor);
        spinnerAddCiclo = (Spinner) findViewById(R.id.spinnerAddCiclo);
        checkboxAddCurso = (CheckBox) findViewById(R.id.checkboxAddCurso);
        checkboxAddProfesor = (CheckBox) findViewById(R.id.checkboxAddProfesor);
        editTextCurso = (EditText) findViewById(R.id.editTextCurso);
        editTextSigla = (EditText) findViewById(R.id.editTextSigla);
        editTextCreditos = (EditText) findViewById(R.id.editTextCreditos);
        editTextProfesor = (EditText) findViewById(R.id.editTextProfesor);
        editTextAñoPc = (EditText) findViewById(R.id.editTextAñoPc);
        botonAñadirCurso = (Button) findViewById(R.id.botonAñadirCurso);
        botonAñadirProfesor = (Button) findViewById(R.id.botonAñadirProfesor);
        botonPrincipal = (Button) findViewById(R.id.botonPrincipal);
        addArchivo = (Button) findViewById(R.id.addArchivo);
        imageBack5 = (ImageButton) findViewById(R.id.imageBack5);

        botonAñadirCurso.setOnClickListener(this);
        botonAñadirProfesor.setOnClickListener(this);
        botonPrincipal.setOnClickListener(this);
        imageBack5.setOnClickListener(this);
        addArchivo.setOnClickListener(this);
        checkboxAddCurso.setOnCheckedChangeListener(this);
        checkboxAddProfesor.setOnCheckedChangeListener(this);

        deshabilitarCurso();
        editTextProfesor.setEnabled(false);
        botonAñadirProfesor.setEnabled(false);

        ArrayAdapter<String> spinnerCursoAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getListaCursos());
        spinnerCurso.setAdapter(spinnerCursoAdapter);
        ArrayAdapter<String> spinnerProfesorAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getlistaProfes());
        spinnerProfesor.setAdapter(spinnerProfesorAdapter);

        getSupportActionBar().setTitle("Cargar Pc");

        Intent i = new Intent(this, SearchActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this,0,i,0);

        String ch_id = "0001";
        builder = new NotificationCompat.Builder(this, ch_id);
        builder.setContentTitle("Registro Exitoso");
        builder.setContentText("Hola mundo");

        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setDefaults(NotificationCompat.DEFAULT_ALL);

        builder.setSmallIcon(R.mipmap.ic_launcher_round);
        builder.setContentIntent(pi);
        builder.setAutoCancel(true);

        editTextAñoPc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String eAño = editTextAñoPc.getText().toString();
                List<String> lista = new ArrayList<String>();
                lista.add(eAño + "-I");
                lista.add(eAño + "-II");
                ArrayAdapter<String> spinnerCicloAdapter = new ArrayAdapter<String>(UploadActivity.this, android.R.layout.simple_spinner_item, lista);
                spinnerAddCiclo.setAdapter(spinnerCicloAdapter);

            }
        });
    }

    private static int REQUEST_CODE_READ_STORAGE_PERMISSION = 2;

    @Override
    public void onClick(View view) {
        String eCurso = editTextCurso.getText().toString();
        String eSigla = editTextSigla.getText().toString();
        String eCreditos = editTextCreditos.getText().toString();

        if (view.getId() == R.id.botonAñadirCurso) {
            if (eCurso.equals("") || eSigla.equals("") || eCreditos.equals("")) {
                Toast.makeText(this, "Complete todos sus datos", Toast.LENGTH_LONG).show();
                if (eCurso.equals("")) {
                    editTextCurso.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eSigla.equals("")) {
                    editTextSigla.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eCreditos.equals("")) {
                    editTextCreditos.setHintTextColor(getResources().getColor(R.color.red));
                }
            }
            else if (validarNameCurso(eCurso) && validarSigla(eSigla)) {
                int cred = Integer.parseInt(eCreditos);
                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(Curso.CAMPO_CURSENAME, eCurso);
                values.put(Curso.CAMPO_SIGLA, eSigla);
                values.put(Curso.CAMPO_CREDITOS, cred);

                db.insert(Curso.TABLA_CURSOS, Curso.CAMPO_ID, values);

                checkboxAddCurso.setChecked(false);
                ArrayAdapter<String> spinnerCursoAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getListaCursos());
                spinnerCurso.setAdapter(spinnerCursoAdapter);

            }
            else {
                if (!validarNameCurso(eCurso)) {
                    Toast.makeText(this, "El Curso ingresado ya exixte", Toast.LENGTH_LONG).show();
                    editTextCurso.setText("");
                    editTextCurso.setHintTextColor(getResources().getColor(R.color.red));
                }
                else if (!validarSigla(eSigla)) {
                    Toast.makeText(this, "Las Siglas ingresadas ya exixten", Toast.LENGTH_LONG).show();
                    editTextSigla.setText("");
                    editTextSigla.setHintTextColor(getResources().getColor(R.color.red));
                }
            }
        }
        else if (view.getId() == R.id.botonAñadirProfesor) {
            String eProfe = editTextProfesor.getText().toString();
            if (eProfe.equals("")) {
                Toast.makeText(this, "Ingrese el nombre del Profesor", Toast.LENGTH_LONG).show();
                editTextProfesor.setHintTextColor(getResources().getColor(R.color.red));
            }
            else if (!getlistaProfes().contains(eProfe)) {
                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(Profesor.CAMPO_DOCENTE, eProfe);

                db.insert(Profesor.TABLA_PROFESORES, Curso.CAMPO_ID, values);

                checkboxAddProfesor.setChecked(false);
                ArrayAdapter<String> spinnerProfesorAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getlistaProfes());
                spinnerProfesor.setAdapter(spinnerProfesorAdapter);
            }
            else {
                Toast.makeText(this, "El Nombre del Profesor ingresado ya exixte", Toast.LENGTH_LONG).show();
                editTextProfesor.setText("");
                editTextProfesor.setHintTextColor(getResources().getColor(R.color.red));
            }
        }
        else if (view.getId() == R.id.botonPrincipal) {
            String itemCurso = spinnerCurso.getSelectedItem()+"";
            String itemProfesor = spinnerProfesor.getSelectedItem()+"";
            String eAño = editTextAñoPc.getText().toString();
            String itemCiclo = spinnerAddCiclo.getSelectedItem()+"";
            itemCiclo = itemCiclo.replaceAll(eAño+"-", "");
            getListaPcs();

            if (eAño.equals("") || blobPc == null) {
                if (eAño.equals("")) {
                    Toast.makeText(this, "Ingrese el Año de la Pc", Toast.LENGTH_LONG).show();
                    editTextAñoPc.setHintTextColor(getResources().getColor(R.color.red));
                }
                else {
                    Toast.makeText(this, "Seleccione el archivo que contiene la Pc", Toast.LENGTH_LONG).show();
                }
            }
            else if (!spinnerCurso.isEnabled() || !spinnerProfesor.isEnabled()) {
                Toast.makeText(this, "Desactive añadir curso y/o profesor", Toast.LENGTH_LONG).show();
            }
            else if (validarPc(new Pcs(itemCurso,itemProfesor,Integer.parseInt(eAño)),itemCiclo)){
                int itemAño = Integer.parseInt(eAño);
                editTextAñoPc.setText("");
                editTextAñoPc.setHintTextColor(getResources().getColor(R.color.gris));
                spinnerAddCiclo.setAdapter(null);

                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(Pcs.CAMPO_SIGLA, idCurso);
                values.put(Pcs.CAMPO_DOCENTE, idProfe);
                values.put(Pcs.CAMPO_AÑO, itemAño);
                values.put(Pcs.CAMPO_CICLO, itemCiclo);
                values.put(Pcs.CAMPO_ARCHIVO, blobPc);

                db.insert(Pcs.TABLA_PCS, Pcs.CAMPO_ID, values);

                Toast.makeText(this, "Su Pc ha sido Registrada, Gracias", Toast.LENGTH_LONG).show();
                blobPc = null;

                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                int id=1;
                builder.setContentText("Gracias por Registrar su Pc, Click aquí para verificar su registro");
                notificationManager.notify(id,builder.build());

            }
            else {
                Toast.makeText(this, "La Pc ingresada ya existe", Toast.LENGTH_LONG).show();
            }
        }
        else if (view.getId() == R.id.imageBack5) {
            Intent i = new Intent(this, HomeActivity.class);
            startActivity(i);
        }

        else if (view.getId() == R.id.addArchivo) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(UploadActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_CODE_READ_STORAGE_PERMISSION);
            } else {
                seleccionarArchivo();
            }
        }

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_READ_STORAGE_PERMISSION && grantResults.length>0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                seleccionarArchivo();
            }
            else {
                Toast.makeText(this,"Permiso denegado",Toast.LENGTH_SHORT).show();
            }

        }

    }

    private void seleccionarArchivo() {
        Intent seleccionar = new Intent(Intent.ACTION_GET_CONTENT);
        seleccionar.setType("*/*");
        seleccionar.addCategory(Intent.CATEGORY_DEFAULT);
        seleccionar = Intent.createChooser(seleccionar,"Elegir Pc");

        if (seleccionar.resolveActivity(getPackageManager())!=null) {
            startActivityForResult(seleccionar, REQUEST_CODE_READ_STORAGE_PERMISSION);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_READ_STORAGE_PERMISSION && resultCode == RESULT_OK) {

            if (data!=null && data.getData()!=null) {

                try {

                    Uri selectedFileUri = data.getData();
                    InputStream in = getContentResolver().openInputStream(selectedFileUri);
                    ByteArrayOutputStream os = new ByteArrayOutputStream();

                    int nRead;
                    blobPc = new byte[16384];

                    while ((nRead = in.read(blobPc,0,blobPc.length)) != -1) {
                        os.write(blobPc,0,nRead);
                    }

                    blobPc = os.toByteArray();

                    Toast.makeText(this,"Se guardó el archivo", Toast.LENGTH_SHORT).show();

                } catch (IOException e) {

                }

            }

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (compoundButton.getId() == R.id.checkboxAddCurso) {
            if (isChecked) {
                habilitarCurso();

            } else {
                deshabilitarCurso();

            }
        }
        if (compoundButton.getId() == R.id.checkboxAddProfesor) {
            if (isChecked) {
                editTextProfesor.setEnabled(true);
                spinnerProfesor.setEnabled(false);
                botonAñadirProfesor.setEnabled(true);

            } else {
                editTextProfesor.setEnabled(false);
                spinnerProfesor.setEnabled(true);
                botonAñadirProfesor.setEnabled(false);
                editTextProfesor.setText("");
                editTextProfesor.setHintTextColor(getResources().getColor(R.color.gris));
            }
        }
    }
    public void deshabilitarCurso () {
        editTextCurso.setEnabled(false);
        editTextSigla.setEnabled(false);
        editTextCreditos.setEnabled(false);
        spinnerCurso.setEnabled(true);
        botonAñadirCurso.setEnabled(false);
        editTextCurso.setText("");
        editTextSigla.setText("");
        editTextCreditos.setText("");
        editTextCurso.setHintTextColor(getResources().getColor(R.color.gris));
        editTextSigla.setHintTextColor(getResources().getColor(R.color.gris));
        editTextCreditos.setHintTextColor(getResources().getColor(R.color.gris));
    }

    public void habilitarCurso () {
        editTextCurso.setEnabled(true);
        editTextSigla.setEnabled(true);
        editTextCreditos.setEnabled(true);
        spinnerCurso.setEnabled(false);
        botonAñadirCurso.setEnabled(true);

    }

    List<Pcs> listaPcs;
    private void getListaPcs() {

        listaPcs = new LinkedList<Pcs>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String sqlCode = "SELECT " + Curso.TABLA_CURSOS+ "."+Curso.CAMPO_SIGLA +", " + Profesor.TABLA_PROFESORES+ "."+Profesor.CAMPO_DOCENTE +", " + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_AÑO +", " + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_CICLO
                + " FROM " + Pcs.TABLA_PCS
                + " INNER JOIN " + Curso.TABLA_CURSOS + " ON "+ Curso.TABLA_CURSOS+ "."+Curso.CAMPO_ID + "=" + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_SIGLA
                + " INNER JOIN " + Profesor.TABLA_PROFESORES + " ON "+ Profesor.TABLA_PROFESORES+ "."+Profesor.CAMPO_ID + "=" + Pcs.TABLA_PCS+ "."+Pcs.CAMPO_DOCENTE +";";
        Cursor cursor = db.rawQuery(sqlCode,null);
        while (cursor.moveToNext()) {
            Pcs p = new Pcs(cursor.getString(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3));
            listaPcs.add(p);
        }
    }

    int idCurso, idProfe;
    public boolean validarPc (Pcs pcsObjeto, String ciclo) {
        boolean e = false;

        for (Pcs p : listaPcs) {
            e = p.igualCursoProfeAño(pcsObjeto) && p.getCiclo().equals(ciclo);
            if (e) break;
        }

        if (!e) {
            for (Profesor lfp : listaFullProfes) {
                if (lfp.getDocente().equals(pcsObjeto.getDocente())) {
                    idProfe=lfp.getId();
                }
            }
            for (Curso c : listaCursos) {
                if (c.getSigla().equals(pcsObjeto.getSigla())) {
                    idCurso=c.getId();
                }
            }
        }
        return !e;
    }

    List<Curso> listaCursos;

    private List<String> getListaCursos() {

        List<String> listaSiglas = new LinkedList<String>();
        listaCursos = new LinkedList<Curso>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Curso.CAMPO_ID, Curso.CAMPO_CURSENAME, Curso.CAMPO_SIGLA, Curso.CAMPO_CREDITOS};
        Cursor cursor = db.query(Curso.TABLA_CURSOS, campos, null,null,null,null,Curso.CAMPO_ID);

        while (cursor.moveToNext()) {
            Curso c = new Curso(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3));
            listaCursos.add(c);
            listaSiglas.add(c.getSigla());
        }
        return listaSiglas;
    }

    public boolean validarNameCurso(String cursename) {
        boolean f = false;
        for (Curso c : listaCursos) {
            f = c.getCursename().equals(cursename);;
            if (f) break;
        }
        return !f;
    }

    public boolean validarSigla(String sigla) {
        boolean f = false;
        for (Curso c : listaCursos) {
            f = c.getSigla().equals(sigla);;
            if (f) break;
        }
        return !f;
    }

    List<String> listaProfes;
    List<Profesor> listaFullProfes;

    private List<String> getlistaProfes() {

        listaFullProfes = new LinkedList<Profesor>();
        listaProfes = new LinkedList<String>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Profesor.CAMPO_ID, Profesor.CAMPO_DOCENTE};
        Cursor cursor = db.query(Profesor.TABLA_PROFESORES, campos, null,null,null,null,Profesor.CAMPO_ID);
        while (cursor.moveToNext()) {
            Profesor p = new Profesor(cursor.getInt(0), cursor.getString(1));
            listaFullProfes.add(p);
            listaProfes.add(cursor.getString(1));
        }
        return listaProfes;
    }
}
package com.example.pcsnow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pcsnow.entidades.Conexion;
import com.example.pcsnow.entidades.Usuario;
import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    EditText modNombre, modApellido, modCorreo, modUsuario, modContraseña, passActual;
    Button botonModificar, botonEliminar;
    Conexion conexion;
    Usuario usuario;
    ImageButton imageBack6;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        conexion = new Conexion(this);

        modNombre = (EditText) findViewById(R.id.modNombre);
        modApellido = (EditText)findViewById(R.id.modApellido);
        modCorreo = (EditText) findViewById(R.id.modCorreo);
        modUsuario = (EditText) findViewById(R.id.modUsuario);
        modContraseña = (EditText) findViewById(R.id.modContraseña);
        passActual = (EditText) findViewById(R.id.passActual);
        botonModificar = (Button) findViewById(R.id.botonModificar);
        botonEliminar = (Button) findViewById(R.id.botonEliminar);
        imageBack6 = (ImageButton) findViewById(R.id.imageBack6);

        botonEliminar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        imageBack6.setOnClickListener(this);

        SharedPreferences sharedPreferences = getSharedPreferences("pcsnow", MODE_PRIVATE);
        String json = sharedPreferences.getString("saveUsuario", "");
        Gson gson = new Gson();
        usuario = gson.fromJson(json, Usuario.class);
        modNombre.setText(usuario.getNombre());
        modApellido.setText(usuario.getApellido());
        modCorreo.setText(usuario.getCorreo());
        modUsuario.setText(usuario.getUsuario());
        modContraseña.setText(usuario.getContraseña());

        getSupportActionBar().setTitle("Configuración");
    }

    @Override
    public void onClick(View view) {
        SQLiteDatabase db = conexion.getWritableDatabase();
        String[] valoresWhere = {usuario.getUsuario()};
        String where = Usuario.CAMPO_USUARIO + "=?";

        String eNombre = modNombre.getText().toString();
        String eApellido = modApellido.getText().toString();
        String eCorreo = modCorreo.getText().toString();
        String eUser = modUsuario.getText().toString();
        String ePassword = modContraseña.getText().toString();
        String eActualPass = passActual.getText().toString();


        if (view.getId() == R.id.botonModificar) {
            getLista();
            if (eNombre.equals("") || eApellido.equals("") || eCorreo.equals("") || eUser.equals("") || ePassword.equals("") || eActualPass.equals("")) {
                Toast.makeText(this, "Complete todos sus datos", Toast.LENGTH_LONG).show();
                if (eNombre.equals("")) {
                    modNombre.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eApellido.equals("")) {
                    modApellido.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eCorreo.equals("")) {
                    modCorreo.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eUser.equals("")) {
                    modUsuario.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (ePassword.equals("")) {
                    modContraseña.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eActualPass.equals("")) {
                    passActual.setHintTextColor(getResources().getColor(R.color.red));
                }
            }
            else if (eActualPass.equals(usuario.getContraseña()) && validarRegistro(eNombre, eApellido, eCorreo, eUser)) {
                ContentValues values = new ContentValues();
                values.put(Usuario.CAMPO_NOMBRE, eNombre);
                values.put(Usuario.CAMPO_APELLIDO, eApellido);
                values.put(Usuario.CAMPO_CORREO, eCorreo);
                values.put(Usuario.CAMPO_USUARIO, eUser);
                values.put(Usuario.CAMPO_CONTRASEÑA, ePassword);

                db.update(Usuario.TABLA_USUARIOS, values, where,valoresWhere);

                SharedPreferences.Editor editor = getSharedPreferences("pcsnow", MODE_PRIVATE).edit();
                Gson gson = new Gson();
                String json = gson.toJson(new Usuario(eNombre,eApellido,eCorreo,eUser,ePassword));
                editor.putString("saveUsuario", json);
                editor.commit();

                Intent i = new Intent(this, HomeActivity.class);
                Toast.makeText(this, "Sus Datos fueron Actualizados", Toast.LENGTH_LONG).show();
                startActivity(i);
            }
            else {
                passActual.setText("");
                passActual.setHintTextColor(getResources().getColor(R.color.gris));

                if (!validarFullName(new Usuario(eNombre, eApellido, eCorreo, eUser))) {
                    Toast.makeText(this, "El Nombre y Apellido ingresado ya existe", Toast.LENGTH_LONG).show();
                    modNombre.setText("");
                    modApellido.setText("");
                    modNombre.setHintTextColor(getResources().getColor(R.color.red));
                    modApellido.setHintTextColor(getResources().getColor(R.color.red));
                }
                else if (!validarCorreo(new Usuario(eNombre, eApellido, eCorreo, eUser))) {
                    Toast.makeText(this, "El Correo ingresado ya existe", Toast.LENGTH_LONG).show();
                    modCorreo.setText("");
                    modCorreo.setHintTextColor(getResources().getColor(R.color.red));
                }
                else if (!validarUsername(new Usuario(eNombre, eApellido, eCorreo, eUser))) {
                    Toast.makeText(this, "El Usuario ingresado ya existe", Toast.LENGTH_LONG).show();
                    modUsuario.setText("");
                    modUsuario.setHintTextColor(getResources().getColor(R.color.red));
                }
                else if (!eActualPass.equals(usuario.getContraseña())) {
                    Toast.makeText(this, "Contraseña Actual Incorrecta", Toast.LENGTH_LONG).show();
                    passActual.setText("");
                    passActual.setHintTextColor(getResources().getColor(R.color.red));
                }
            }
        }
        else if (view.getId() == R.id.botonEliminar) {
            if (eActualPass.equals(usuario.getContraseña())) {
                db.delete(Usuario.TABLA_USUARIOS, where,valoresWhere);

                Intent i = new Intent(this, MainActivity.class);
                SharedPreferences.Editor editor = getSharedPreferences("pcsnow", MODE_PRIVATE).edit();
                editor.putBoolean("closedSesion", true);
                editor.commit();
                Toast.makeText(this, "Su Cuenta ha sido Eliminada", Toast.LENGTH_LONG).show();
                startActivity(i);
            }
            else {
                Toast.makeText(this, "Contraseña Actual Incorrecta", Toast.LENGTH_LONG).show();
                passActual.setText("");
                passActual.setHintTextColor(getResources().getColor(R.color.red));
            }
        }
        else if (view.getId() == R.id.imageBack6) {
            Intent i = new Intent(this, HomeActivity.class);
            startActivity(i);
        }
    }
    List<Usuario> listaUsuarios;
    private void getLista() {

        listaUsuarios = new LinkedList<Usuario>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Usuario.CAMPO_NOMBRE, Usuario.CAMPO_APELLIDO, Usuario.CAMPO_CORREO, Usuario.CAMPO_USUARIO};
        Cursor cursor = db.query(Usuario.TABLA_USUARIOS, campos, null,null,null,null,Usuario.CAMPO_ID);

        while (cursor.moveToNext()) {
            Usuario u = new Usuario(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
            listaUsuarios.add(u);
        }
    }

    public boolean validarFullName(Usuario usuario) {
        boolean valNombres = false;
        for (Usuario u : listaUsuarios) {
            valNombres = u.igualRegistro(usuario) && !u.igualRegistro(this.usuario);
            if (valNombres) break;
        }
        return !valNombres;
    }

    public boolean validarCorreo(Usuario usuario) {
        boolean valCorreo = false;
        for (Usuario u : listaUsuarios) {
            valCorreo = u.getCorreo().equals(usuario.getCorreo()) && !u.getCorreo().equals(this.usuario.getCorreo());;
            if (valCorreo) break;
        }
        return !valCorreo;
    }

    public boolean validarUsername(Usuario usuario) {
        boolean valUsername = false;
        for (Usuario u : listaUsuarios) {
            valUsername = u.getUsuario().equals(usuario.getUsuario()) && !u.getUsuario().equals(this.usuario.getUsuario());
            if (valUsername) break;
        }
        return !valUsername;
    }

    public boolean validarRegistro(Usuario usuario) {
        return (validarFullName(usuario) && validarCorreo(usuario) && validarUsername(usuario));
    }
    public boolean validarRegistro(String nombre, String apellido, String correo, String usuario){
        return validarRegistro(new Usuario(nombre, apellido, correo, usuario));
    }
}
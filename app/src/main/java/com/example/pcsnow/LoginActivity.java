package com.example.pcsnow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pcsnow.entidades.Conexion;
import com.example.pcsnow.entidades.Usuario;
import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    Conexion conexion;
    private Button botonIniciar;
    private EditText editUsuario, editContraseña;
    private ImageButton imageBack2;
    private CheckBox saveLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        botonIniciar = (Button) findViewById(R.id.botonIniciar);
        editUsuario = (EditText)findViewById(R.id.editUsuario);
        editContraseña = (EditText) findViewById(R.id.editContraseña);
        imageBack2 = (ImageButton) findViewById(R.id.imageBack2);
        saveLog = (CheckBox) findViewById(R.id.saveLog);

        botonIniciar.setOnClickListener(this);
        imageBack2.setOnClickListener(this);

        getSupportActionBar().setTitle(getResources().getString(R.string.login));

        SharedPreferences sharedPreferences = getSharedPreferences("pcsnow", MODE_PRIVATE);
        Boolean lastCheck = sharedPreferences.getBoolean("lastCheck", false);
        saveLog.setChecked(lastCheck);
        if (lastCheck) {
            String lastUser = sharedPreferences.getString("lastUser", "");
            String lastPass = sharedPreferences.getString("lastPass", "");
            editContraseña.setBackgroundColor(getResources().getColor(R.color.blueClaro));
            editUsuario.setBackgroundColor(getResources().getColor(R.color.blueClaro));
            editUsuario.setText(lastUser);
            editContraseña.setText(lastPass);
        }

        conexion = new Conexion(this);
    }

    @Override
    public void onClick(View view) {

        String eUsuario = editUsuario.getText().toString();
        String eContraseña = editContraseña.getText().toString();
        getLista();

        if (view.getId() == R.id.botonIniciar) {

            if (eUsuario.equals("") || eContraseña.equals("")) {
                Toast.makeText(this, "Complete todos sus datos", Toast.LENGTH_LONG).show();
                if (eUsuario.equals("")) {
                    editUsuario.setHintTextColor(getResources().getColor(R.color.red));
                }
                if (eContraseña.equals("")) {
                    editContraseña.setHintTextColor(getResources().getColor(R.color.red));
                }
            }

            else if (validarUsuario(new Usuario(eUsuario,eContraseña))) {
                Intent i = new Intent(this, HomeActivity.class);
                String nombre = obtenerDatos(eUsuario);
                Usuario usuario = new Usuario(nombre,apelllido,correo,eUsuario,eContraseña);

                SharedPreferences.Editor editor = getSharedPreferences("pcsnow", MODE_PRIVATE).edit();
                Gson gson = new Gson();
                String json1 = gson.toJson(usuario);
                editor.putString("saveUsuario", json1);
                editor.commit();

                startActivity(i);
                }

            else {
                Toast.makeText(this, "Usuario y/o contraseña incorrectos", Toast.LENGTH_LONG).show();
                editContraseña.setText("");
                editUsuario.setText("");
                editUsuario.setHintTextColor(getResources().getColor(R.color.red));
                editContraseña.setHintTextColor(getResources().getColor(R.color.red));
                }

            SharedPreferences.Editor editor = getSharedPreferences("pcsnow", MODE_PRIVATE).edit();
            editor.putString("lastUser",eUsuario);
            editor.putString("lastPass", eContraseña);
            editor.putBoolean("lastCheck",saveLog.isChecked());
            editor.commit();
        }
        else if(view.getId() == R.id.imageBack2) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }

    }
    List<Usuario> listaUsuarios;

    private void getLista() {

        listaUsuarios = new LinkedList<Usuario>();
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Usuario.CAMPO_NOMBRE, Usuario.CAMPO_APELLIDO, Usuario.CAMPO_CORREO, Usuario.CAMPO_USUARIO, Usuario.CAMPO_CONTRASEÑA};
        Cursor cursor = db.query(Usuario.TABLA_USUARIOS, campos, null,null,null,null,Usuario.CAMPO_ID);

        while (cursor.moveToNext()) {
            Usuario u = new Usuario(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
            listaUsuarios.add(u);
        }
    }
    public boolean validarUsuario(Usuario usuario) {
        boolean e = false;

        for (Usuario u : listaUsuarios) {
            e = u.igualLogin(usuario);
            if (e) break;
        }

        return e;
    }
    String apelllido, correo;
    public String obtenerDatos(String username) {

        String nombre = "";
        for (Usuario u : listaUsuarios) {
            nombre = u.getNombre();
            apelllido = u.getApellido();
            correo = u.getCorreo();
            if (u.getUsuario().equals(username)) break;
        }
        return nombre;
    }
}